<?php
declare(strict_types=1);

require_once __DIR__.'/../vendor/autoload.php'; // Autoload files using Composer

define('XMTK_XML_DOCTYPE', "<?xml version='1.0'?>");
define('XMTK_EXPECTED_SINGLETAG', '<tag>1</tag>');

define('XMTK_EXPECTED_INPARENT', '<item>
    <title>example</title>
</item>');

define('XMTK_EXPECTED_SIBLINGPARENTS', '<root>
    <info>
        <deeper>
            <note>interesting</note>
            <note2>interesting too</note2>
        </deeper>
        <sibling>
            <child>value</child>
        </sibling>
    </info>
</root>');

define('XMTK_EXPECTED_TWOPARENTLESS', '<tag>1</tag>
<node>2</node>');

define('XMTK_EXPECTED_COLLECTIONROOT', '<collection>
    <item>salad</item>
    <item>napkins</item>
    <item>insecticide</item>
</collection>');

define('XMTK_EXPECTED_PARENTSCOLLECTION', '<users>
    <user>
        <name>Ivan</name>
    </user>
    <user>
        <name>Nikolay</name>
    </user>
</users>');

define('XMTK_EXPECTED_NODEINCOLLECTION', '<root>
    <users>
        <user>
            <name>ivan</name>
            <level>1</level>
        </user>
        <user>
            <name>vasya</name>
            <level>2</level>
        </user>
        <user>
            <name>peter</name>
            <level>4</level>
        </user>
        <admin>
            <name>root</name>
            <level>3</level>
        </admin>
    </users>
    <info>
        <note>interesting</note>
    </info>
</root>');

define('XMTK_EXPECTED_WRAPPERCOLLECTION', '<parent>
    <number>1</number>
    <number>0</number>
</parent>');

use PHPUnit\Framework\TestCase;
use Xmtk\Writer;

/* overrides some methods of Writer */
class MyWriter extends Writer {
	public $err_msg;
	protected function error(string $message) {
		$this->err_msg = $message;
	} // error()
} // MyWriter

function expected($xml) {
	return XMTK_XML_DOCTYPE.PHP_EOL.$xml.PHP_EOL;
} // expected()

/* Covers Writer */
final class WriterTests extends TestCase {
	private $writer;

	protected function setUp(): void {
		$this->writer = new Writer;
	} // setUp()
	function testWriterCanBeCreated() {
		$this->assertInstanceOf(Writer::class, $this->writer);
	} // writer can be created
	function testCanWriteSingleTag() {
		$this->assertEquals(expected(XMTK_EXPECTED_SINGLETAG),
			$this->writer->xmlWriteFromArray(['tag' => 1]));
	} // can write single tag
	function testCanWriteValueInsideParent() {
		$this->assertEquals(expected(XMTK_EXPECTED_INPARENT),
			$this->writer->xmlWriteFromArray(['item' =>
				['title' => 'example']]));
	} // can write value inside parent
	function testCanWriteSiblingParents() {
		$this->assertEquals(expected(XMTK_EXPECTED_SIBLINGPARENTS),
			$this->writer->xmlWriteFromArray(['root' =>
				['info' =>
					['deeper' =>
						['note' => 'interesting',
						 'note2' => 'interesting too'],
					 'sibling' =>
						['child' => 'value']]]]));
	} // can write sibling parents
	function testCanWriteFirstTagOfTwoParentless() {
		$this->assertEquals(expected(XMTK_EXPECTED_TWOPARENTLESS),
			$this->writer->xmlWriteFromArray(['tag' => 1, 'node' => 2]));
	} // can write first tag of two parentless
	function testCanWriteCollectionRoot() {
		$this->assertEquals(expected(XMTK_EXPECTED_COLLECTIONROOT),
			$this->writer->xmlWriteFromArray(['collection' =>
				['item' =>
					['salad', 'napkins', 'insecticide']]]));
	} // can write collection root
	function testCanWriteCollectionOfParents() {
		$this->assertEquals(expected(XMTK_EXPECTED_PARENTSCOLLECTION),
			$this->writer->xmlWriteFromArray(['users' =>
				['user' =>
					[['name' => 'Ivan'],
					 ['name' => 'Nikolay']]]]));
	} // can write collection parents
	function testCanWriteCollectionWithNodeInside() {
		$this->assertEquals(expected(XMTK_EXPECTED_NODEINCOLLECTION),
			$this->writer->xmlWriteFromArray(['root' =>
				['users' =>
					['user' =>
						[['name' => 'ivan', 'level' => 1],
						 ['name' => 'vasya', 'level' => 2],
						 ['name' => 'peter', 'level' => 4]],
					 'admin' =>
					 	['name' => 'root', 'level' => 3]],
			 	'info' => ['note' => 'interesting']]]));
	} // can write collection with node inside
	function testCanWriteWrappedCollection() {
		$this->assertEquals(expected(XMTK_EXPECTED_WRAPPERCOLLECTION),
			$this->writer->xmlWriteFromArray(['parent' =>
				['number' => [1, 0]]]));
	} // can write wrapped collection
	function testCanGetErrorMessageForCustomHandling() {
		$my_writer = new MyWriter;
		$my_writer->xmlWriteFromArray(NULL);
		$this->assertEquals(
			'\\Xmtk\\Writer: xmlWriteFromArray() expects an array.',
			$my_writer->err_msg);
	} // can handle error
	function testReturnsFalseAfterError() {
		$my_writer = new MyWriter;
		$xml = $my_writer->xmlWriteFromArray('Not an XML');
		$this->assertEquals('', $xml);
	} // writer returns FALSE after error handled
} // class WriterTests

?>

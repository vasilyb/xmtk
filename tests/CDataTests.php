<?php
declare(strict_types=1);

require_once __DIR__.'/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Xmtk\CData;

/* Covers CData */
final class CDataTests extends TestCase {
	private $coder;

	protected function setUp(): void {
		$this->coder = new CData;
	} // setUp()

	function testCDataEncoderCanBeCreated() {
		$this->assertInstanceOf(CData::class, $this->coder);
	} // CData encoder can be created
	function testCanEncodeToCData() {
		$str = '<b>Some markup</b>';
		$this->assertEquals("<![CDATA[$str]]>",
			$this->coder->encode($str));
	} // can encode to CData
} // CDataTests
?>

<?php
declare(strict_types=1);

require_once __DIR__.'/../vendor/autoload.php'; // Autoload files using Composer

use PHPUnit\Framework\TestCase;
use Xmtk\Parser;

class MyParser extends Parser {
	public $err_msg = '';
	function error(string $message) {
		$this->err_msg = $message;
	} // error()
} // this class overrides error handling

/* Covers Parser */
final class ParserTests extends TestCase {
	private $parser;
	
	protected function setUp(): void {
		$this->parser = new Parser;
	} // setting up the fixture
	function testParserCanBeCreated() {
		$this->assertInstanceOf(Parser::class,
			$this->parser);
	} // parser can be created
	function testCanParseSingleTag() {
		$value = $this->parser->xmlParseIntoArray('<tag>one</tag>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('tag', $value);
		$this->assertEquals('one', $value['tag']);
	} // can parse single tag
	function testCanParseTagInsideAnother() {
		$value = $this->parser->xmlParseIntoArray('
			<item>
				<title>example</title>
			</item>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('item', $value);
		$item = $value['item'];
		$this->assertTrue(is_array($item));
		$this->assertEquals(1, count($item));
		$this->assertArrayHasKey('title', $item);
		$this->assertEquals('example', $item['title']);
	} // can parse tag inside another
	function testCanParseSiblingParents() {
		$value = $this->parser->xmlParseIntoArray('
			<root>
			  <info>
				<deeper>
				  <note>interesting</note>
				  <note2>interesting too</note2>
				</deeper>
				<sibling>
				  <child>value</child>
				</sibling>
			  </info>
			</root>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('root', $value);
		$root = $value['root'];
		$this->assertTrue(is_array($root));
		$this->assertEquals(1, count($root));
		$this->assertArrayHasKey('info', $root);
		$info = $root['info'];
		$this->assertTrue(is_array($info));
		$this->assertEquals(2, count($info));
		$this->assertArrayHasKey('deeper', $info);
		$deeper = $info['deeper'];
		$this->assertTrue(is_array($deeper));
		$this->assertEquals(2, count($deeper));
		$this->assertArrayHasKey('note', $deeper);
		$this->assertEquals('interesting', $deeper['note']);
		$this->assertArrayHasKey('note2', $deeper);
		$this->assertEquals('interesting too', $deeper['note2']);
		$this->assertArrayHasKey('sibling', $info);
		$sibling = $info['sibling'];
		$this->assertTrue(is_array($sibling));
		$this->assertEquals(1, count($sibling));
		$this->assertArrayHasKey('child', $sibling);
		$this->assertEquals('value', $sibling['child']);
	} // can parse sibling parents
	function testCanParseFirstTagOfTwoWithoutRoot() {
		//it's all right here, parser parses only the 1st root <tag>
		$value = $this->parser->xmlParseIntoArray('<tag>1</tag><node>2</node');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('tag', $value);
		$this->assertEquals('1', $value['tag']);
	} // can parse first tag of two without root
	function testCanParseCollection() {
		$value = $this->parser->xmlParseIntoArray('
			<collection>
				<item>salad</item>
				<item>napkins</item>
				<item>insecticide</item>
			</collection>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('collection', $value);
		$collection = $value['collection'];
		$this->assertTrue(is_array($collection));
		$this->assertEquals(1, count($collection));
		$this->assertArrayHasKey('item', $collection);
		$items = $collection['item'];
		$this->assertEquals(3, count($items));
		$this->assertEquals('salad', $items[0]);
		$this->assertEquals('napkins', $items[1]);
		$this->assertEquals('insecticide', $items[2]);
	} // can parse collection
	function testCanParseParentsCollection() {
		$value = $this->parser->xmlParseIntoArray('
			<users>
			  <user>
				<name>Ivan</name>
			  </user>
			  <user>
				<name>Nikolay</name>
			  </user>
			</users>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('users', $value);
		$users_collection = $value['users'];
		$this->assertTrue(is_array($users_collection));
		$this->assertEquals(1, count($users_collection));
		$this->assertArrayHasKey('user', $users_collection);
		$users = $users_collection['user'];
		$this->assertTrue(is_array($users));
		$this->assertEquals(2, count($users));
		$ivan = $users[0];
		$this->assertTrue(is_array($ivan));
		$this->assertEquals(1, count($ivan));
		$this->assertArrayHasKey('name', $ivan);
		$this->assertEquals('Ivan', $ivan['name']);
		$nikolay = $users[1];
		$this->assertTrue(is_array($nikolay));
		$this->assertEquals(1, count($nikolay));
		$this->assertArrayHasKey('name', $nikolay);
		$this->assertEquals('Nikolay', $nikolay['name']);
	} // can parse parents collection
	function testCanParseCollectionOfDifferentTags() {
		$value = $this->parser->xmlParseIntoArray('
			<root>
			  <users>
				<user>
				  <name>ivan</name>
				  <level>1</level>
				</user>
				<user>
				  <name>vasya</name>
				  <level>2</level>
				</user>
				<admin>
				  <name>root</name>
				  <level>3</level>
				</admin>
				<user>
				  <name>peter</name>
				  <level>4</level>
				</user>
			  </users>
			  <info>
				<note>interesting</note>
			  </info>
			</root>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('root', $value);
		$root = $value['root'];
		$this->assertTrue(is_array($root));
		$this->assertEquals(2, count($root));
		$this->assertArrayHasKey('users', $root);
		$users_collections = $root['users'];
		$this->assertTrue(is_array($users_collections));
		$this->assertEquals(2, count($users_collections));
		$this->assertArrayHasKey('user', $users_collections);
		$this->assertArrayHasKey('admin', $users_collections);
		$users = $users_collections['user'];
		$this->assertTrue(is_array($users));
		$this->assertEquals(3, count($users));
		$ivan = $users[0];
		$this->assertTrue(is_array($ivan));
		$this->assertEquals(2, count($ivan));
		$this->assertArrayHasKey('name', $ivan);
		$this->assertArrayHasKey('level', $ivan);
		$this->assertEquals('ivan', $ivan['name']);
		$this->assertEquals('1', $ivan['level']);
		$vasya = $users[1];
		$this->assertTrue(is_array($vasya));
		$this->assertEquals(2, count($vasya));
		$this->assertArrayHasKey('name', $vasya);
		$this->assertArrayHasKey('level', $vasya);
		$this->assertEquals('vasya', $vasya['name']);
		$this->assertEquals('2', $vasya['level']);
		$peter = $users[2];
		$this->assertTrue(is_array($peter));
		$this->assertEquals(2, count($peter));
		$this->assertArrayHasKey('name', $peter);
		$this->assertArrayHasKey('level', $peter);
		$this->assertEquals('peter', $peter['name']);
		$this->assertEquals('4', $peter['level']);
		$admin = $users_collections['admin'];
		$this->assertTrue(is_array($admin));
		$this->assertEquals(2, count($admin)); // if the tag is the sole then it is not a collection
		$this->assertArrayHasKey('name', $admin);
		$this->assertEquals('root', $admin['name']);
		$this->assertArrayHasKey('level', $admin);
		$this->assertEquals('3', $admin['level']);
		$this->assertArrayHasKey('info', $root);
		$info = $root['info'];
		$this->assertTrue(is_array($info));
		$this->assertEquals(1, count($info));
		$this->assertArrayHasKey('note', $info);
		$this->assertEquals('interesting', $info['note']);
	} // can parse collection of different tags
	function testCanParseRootCollection() {
		$value = $this->parser->xmlParseIntoArray('
			<parent>
				<number>1</number>
				<number>0</number>
			</parent>');
		$this->assertTrue(is_array($value));
		$this->assertEquals(1, count($value));
		$this->assertArrayHasKey('parent', $value);
		$parent = $value['parent'];
		$this->assertTrue(is_array($parent));
		$this->assertEquals(1, count($parent));
		$this->assertArrayHasKey('number', $parent);
		$numbers = $parent['number'];
		$this->assertEquals(2, count($numbers));
		$this->assertEquals(1, $numbers[0]);
		$this->assertEquals(0, $numbers[1]);
	} // can parse root collection
	function testCanReturnEmptyArrayIfXmlIsInvalid() {
		$value = $this->parser->xmlParseIntoArray(
			'invalid XML');
		$this->assertTrue(is_array($value));
		$this->assertEquals(0, count($value));
	} // returns an empty array is XML is invalid
} // class ParserTest
?>


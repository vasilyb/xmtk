<?php
declare(strict_types=1);

namespace Xmtk;

class CData {
	function encode(string $str) {
		return "<![CDATA[$str]]>";
	} // encode()
} // CData

?>

<?php
declare(strict_types=1);

namespace Xmtk; // Yet anoter XML writer (vasily.blinkov@gmail.com)

define('XMTK_DOCTYPE', "<?xml version='1.0'?>");
define('XMTK_INDENT_SIZE', 4);
define('XMTK_INDENTATION', str_repeat(' ', XMTK_INDENT_SIZE));

class Writer extends Composite {

	private function xmlChild(string $tag, $array, int $level): string {
		$xml = $this->xmlFragmentFromArray($array, $level+1);
		return str_repeat(XMTK_INDENTATION, $level).
			"<$tag>".PHP_EOL."$xml".
			str_repeat(XMTK_INDENTATION, $level).
			"</$tag>".
			PHP_EOL;
	} // xmlChild()

	private function xmlCollection(string $tag, $array, int $level): string {
		$xml = '';
		foreach ($array as $element):
			$xml .= $this->writeElement($tag, $element, $level);
		endforeach; // collection $element
		return $xml;
	} // xmlCollection()

	private function xmlValue(string $tag, $value, int $level): string {
		return str_repeat(XMTK_INDENTATION, $level).
			"<$tag>$value</$tag>".
			PHP_EOL;
	} // xmlValue();

	private function writeElement(string $tag, $value, int $level): string {
		if (! is_array($value)): // value
			$xml = $this->xmlValue($tag, $value, $level);
		elseif (count($value) !== 0 && is_numeric(array_keys($value)[0])): // collection
			$xml = $this->xmlCollection($tag, $value, $level);
		else: // child
			$xml = $this->xmlChild($tag, $value, $level);
		endif; // write node
		return $xml;
	} // writeElement()

	private function xmlFragmentFromArray($array, int $level): string {
		$xml = '';
		foreach (array_keys($array) as $tag):
			$xml .= $this->writeElement($tag, $array[$tag], $level);
		endforeach; // $tag in $array
		return $xml;
	} // xmlFragmentFromArray()

	function xmlWriteFromArray($array): string {
		if (! is_array($array)):
			$this->error(
				'\\Xmtk\\Writer: xmlWriteFromArray() expects an array.');
			return '';
		endif; // invalid argument

		return XMTK_DOCTYPE.PHP_EOL.
			$this->xmlFragmentFromArray($array, 0);
	} // xmlWriteFromArray()

} // class Writer

?>

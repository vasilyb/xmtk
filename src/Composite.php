<?php
declare(strict_types=1);

namespace Xmtk;

/* Infrastructure parent class. Composition over inheritance. */
class Composite {

	private $error_handler;

	function __construct() {
		$this->error_handler = new ErrorHandler;
	} // __construct()

	protected function error(string $message) {
		$this->error_handler->error($message);
	} // error()

} // Composite

?>
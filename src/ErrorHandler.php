<?php
declare(strict_types=1);

namespace Xmtk;

class ErrorHandler {
	public function error(string $message) {
		die($message.PHP_EOL);
	} // error()
} // ErrorHandler

?>

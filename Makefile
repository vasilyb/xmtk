parser-tests:
	cd tests/ && phpunit ParserTests.php

writer-tests:
	cd tests/ && phpunit WriterTests.php

cdata-tests:
	cd tests/ && phpunit CDataTests.php

tests: parser-tests writer-tests cdata-tests


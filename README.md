# CHANGELOG

2017-12-24. Overridable errors handling.

2017-12-02. Add the `\Xmtk\CData` class.

2017-11-26. The unit testing using
[GitLab CI](https://gitlab.com/help/ci/README.md) and
[PHPUnit](https://phpunit.de/index.html) was implemented.

2017-09-17. *Breaking change:* the `xml_parse_into_array()` method of the `Parser` class
has been renamed to `xmlParseIntoArray()` according to PHP-FIG's PSR.

2017-09-30. *Introduced the `Xmtk\Writer` class* to write XML from arrays.

# ABOUT

## What `Xmtk` is?

### xml parser

`Xmtk` is a wrapper around the `xml_parse_into_struct()` function.
It parses XML strings into the structs using the above function,
but then transforms its result into easy-to-handle array hierarchies.
Please, see example 1 to get the difference.

### xml writer

The `\Xmtk\Writer` class writes hierarchical arrays to XML files.
In other words `Writer` aimed to do the reverse work of `Parser`.
See example 2.


### cdata

There is the `\Xmtk\CData` class in the package.
It offers the `encode()` method to convert strings to CData forms.
See the 3rd example to learn why this class useful.

### example 1

This example shows the difference between the result.
Look at the listing 1. It is the input XML.
Listing 2 shows what result the `xml_parse_into_struct()` will return.
And, finally, listing 3 is the result of `\Xmtk\Parser` work.

### example 2

`\Xmtk\Writer` will convert the arrays looking like in the listing 3 below
into XML string like shown in the listing 1.

### example 3

Let's suppose you need to put HTML markup into the some tag of XML.
The next code will not work correctly.

```php
$writer = new \Xmtk\Writer;
$xml = $writer->xmlWriteFromArray(
	['markup'=>'<strong>text</strong>']);
```
The value of the `$xml` variable in this example will be:
`<markup><strong>text</strong></markup>`.
This is wrong. To fix this situation you may use the `\Xmtk\CData`
class next way.

```php
$cdata = new \Xmtk\CData;
$xml = $writer->xmlWriteFromArray(
	['markup'=>$cdata->encode('<strong>text</strong>')]);
```

The output XML will look like the next one.

```xml
<markup><![CDATA[<strong>text</strong>]]></markup>
```

### listing 1 (input xml)

```xml
<bike>
	<wheel>front</wheel>
	<wheel>rear</wheel>
	<chain>
		<length>1</length>
	</chain>
</bike>
```

### listing 2 (php function)

```php
Array
(
    [0] => Array
        (
            [tag] => bike
            [type] => open
            [level] => 1
            [value] =>

        )

    [1] => Array
        (
            [tag] => wheel
            [type] => complete
            [level] => 2
            [value] => front
        )

    [2] => Array
        (
            [tag] => bike
            [value] =>

            [type] => cdata
            [level] => 1
        )

    [3] => Array
        (
            [tag] => wheel
            [type] => complete
            [level] => 2
            [value] => rear
        )

    [4] => Array
        (
            [tag] => bike
            [value] =>

            [type] => cdata
            [level] => 1
        )

    [5] => Array
        (
            [tag] => chain
            [type] => open
            [level] => 2
            [value] =>

        )

    [6] => Array
        (
            [tag] => length
            [type] => complete
            [level] => 3
            [value] => 1
        )

    [7] => Array
        (
            [tag] => chain
            [value] =>

            [type] => cdata
            [level] => 2
        )

    [8] => Array
        (
            [tag] => chain
            [type] => close
            [level] => 2
        )

    [9] => Array
        (
            [tag] => bike
            [value] =>

            [type] => cdata
            [level] => 1
        )

    [10] => Array
        (
            [tag] => bike
            [type] => close
            [level] => 1
        )

)
```

### listing 3 (parser wrapper)

```php
Array
(
    [bike] => Array
        (
            [wheel] => Array
                (
                    [0] => front
                    [1] => rear
                )

            [chain] => Array
                (
                    [length] => 1
                )

        )

)
```

## What `Xmtk` stands for?

`Xmtk` stands for eXtensible Markup Tool-Kit.
Thanks for your interest.

# USAGE

## Requirements

In November, 2017 the project has been migrated to PHP 7.0. This migration was
intended to avoid usage of legacy PHPUnit.

## Setup

```bash
composer require xmtk/xmtk
```

## `\Xmtk\Parser`

### php

```php
#!/usr/local/bin/php
<?php
require_once __DIR__.'/vendor/autoload.php';

$parser = new \Xmtk\Parser;

$xml = '<bike>
	<wheels>
		<wheel>The front one</wheel>
		<wheel>The rear one</wheel>
	</wheels>
	<chain>
		<count>191</count>
	</chain>
</bike>';

$result = $parser->xmlParseIntoArray($xml);
print_r($result);

?>
```

### output

```php
Array
(
    [bike] => Array
        (
            [wheels] => Array
                (
                    [wheel] => Array
                        (
                            [0] => The front one
                            [1] => The rear one
                        )

                )

            [chain] => Array
                (
                    [count] => 191
                )

        )

)
```

## `\Xmtk\Writer`

The `\Xmtk\Writer` class accepts arrays in the same format as the `\Xmtk\Parser` returns.
See below for the usage example.

```php
require_once __DIR__.'/../vendor/autoload.php';
$writer = new \Xmtk\Writer;
$array = ['collection' => ['item' => ['salad', 'napkins', 'insecticide']]];
$xml = $writer->xmlWriteFromArray($array);
print_r($xml);
/* // Output.
<?xml version='1.0'?>
<collection>
    <item>salad</item>
    <item>napkins</item>
    <item>insecticide</item>
</collection>
*/
```

## Error Handling

Instances of the  `\Xmtk\Parser` class returns empty arrays
(`array()`) if you
passed invalid XML string, or arrays with parsed beginnings of
XML strings with valid start but invalid continuation. In other words
the instances of the `Parser` always return at least empty arrays in
case of error.

Instances of the `\Xmtk\Writer` will interrupt execution of your
scripts in case of errors with the `exit()` PHP construct.
For example if you will pass non-array variable to the
`xmlWriteFromArray()` method of the `Writer` instance.
At the moment of writing this documentation the `\Xmtk\Writer`
offered the possibility to override this behavior.

To implement custom error handling you just need to
implement a descendant class inherit it from the `\Xmtk\Writer`
and override its `error()` method. This method gets a single
parameter `$message` of type `string`. It holds the error message.
The example is below.

### implementing a writer with custom error handling

```php
class MyWriter extends Writer {
	public $err_msg;
	protected function error(string $message) {
		$this->err_msg = $message;
	}
}
```

### unit test to check if the error handling implemented properly

### `\Xmtk\Writer->xmlWriteFromArray()` return value

The `\Xmtk\Writer->xmlWriteFromArray()` method returns `string`
values when passed array structure of the XML document had been
serialized properly or empty string (`''`) otherwise.

```php
function testCanGetErrorMessageForCustomHandling() {
	$my_writer = new MyWriter;
	$my_writer->xmlWriteFromArray(NULL);
	$this->assertEquals(
		'\\Xmtk\\Writer: xmlWriteFromArray() expects an array.',
		$my_writer->err_msg);
}
```

### class diagram

![cd](https://gitlab.com/vasilyb/xmtk/uploads/dc630c449f990634ae57ab1ee8a51843/cd.png)

As you can see, it is possible to override error handling in
the descendants of the `Parser` class too. It may have effect in
a future.

## IMPORTANT NOTES

## arrays

The `\Xmtk\Parser` treats repeating tags placed on the same level as arrays.
This means that if you have two or more `<bar/>` tags inside the `<foo/>` node,
then the array for the `foo` tag will have child array `bar` indexed by numbers.
Look at the `<wheel/>` tags in the listing 1 and how they were processed by
the `\Xmtk\Parser` in the listing 3.

## invalid xml

Results of `\Xmtk\Parser` work in cases when XML is invalid or
partially valid will be the same as of the underlying
`xml_parse_into_struct()` function: empty arrays (when XML is
invalid) or arrays with the parsed beginning of the XML (in cases
when the beginning part of XML is valid).

For example:

### invalid xml

Unit test:

```php
function testCanReturnEmptyArrayIfXmlIsInvalid() {
	$value = $this->parser->xmlParseIntoArray(
		'invalid XML');
	$this->assertTrue(is_array($value));
	$this->assertEquals(0, count($value));
	}
```

The value of the `$value` variable is:

```php
Array
(
)
```

### partially valid xml

Unit test:

```php
function testCanParseFirstTagOfTwoWithoutRoot() {
	$value = $this->parser->xmlParseIntoArray(
		'<tag>1</tag><node>2</node');
	$this->assertTrue(is_array($value));
	$this->assertEquals(1, count($value));
	$this->assertArrayHasKey('tag', $value);
	$this->assertEquals('1', $value['tag']);
}
```

Result of the `$this->parser->xmlParseIntoArray()` is:

```php
Array
(
    [tag] => 1
)
```

## KNOWN ISSUES

### attributes

All we love XML attributes.
But support of this important XML specification part is not implemented yet.
Contribute if you see that `Xmtk` is cool!

# DEVELOPMENT

## Tools

- PL: Xmtk written in the PHP7 programming language. You may need to install
the next packages (FreeBSD 10.x): `php70`, `php70-filter`, `php70-hash`, `php70-iconv`,
`php70-json`, `php70-openssl`, `php70-phar`, `php70-xml`, `php70-zlib`.
- IDE: Not used (no projects like `nbproject/`).
- UT: PHPUnit is the unit-testing framework used. Install it according to the
official [manual](https://phpunit.de/getting-started.html).

### composer

Composer is used as an application level package manager.
You may want to install Composer to your system for more convenient development process.
For FreeBSD the command to setup Composer system-widely will look like the next one.
```bash
sudo pkg install php-composer
```

### tests

This package uses [PHPUnit](https://phpunit.de/index.html) for unit testing.
[PHPUnit](https://phpunit.de/index.html) should be installed to the `$PATH`.

To run test you can use `make`. There are several targets in the
[Makefile](Makefile).

1. `parser-tests` is to run only unit tests to test the `Parser` class.
1. `writer-tests` is to run only unit tests to test the `Writer` class.
1. `cdata-tests` is to run only unit tests to test the `CData` class.
1. `tests` is to run all the unit testing targets defined in the `Makefile`: `parser-tests`, `writer-tests`, `cdata-tests` etc.

#### EXAMPLES

```bash
make tests
make parser-tests
```

